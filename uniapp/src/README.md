## GoEasy Websocket Uniapp Vue3 直播间弹幕和聊天室示例运行步骤

本示例，可以编译为H5、ios app和android ios和微信小程序，以及其他各种小程序

### 免费获取appkey
1. 访问[GoEasy官网](https://www.goeasy.io)进行注册
2. 登陆后，创建一个应用
3. 进入应用详情，即可看到自己的appkey

### 替换appkey
打开main.js，找到初始化GoEasy的地方，将appkey替换成您应用的common key


### 小程序注意事项
如果编译为微信小程序，需要登录微信公众平台->微信小程序开发设置->服务器域名, 添加以下所有5个socket合法域名：  
wss://1hangzhou.goeasy.io  
wss://2hangzhou.goeasy.io  
wss://3hangzhou.goeasy.io  
wss://4hangzhou.goeasy.io  
wss://5hangzhou.goeasy.io

### 体验
建议可以同时运行到多个终端（比如h5或app），体验多个客户端之间互动。
