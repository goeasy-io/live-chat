package io.goeasy.livechat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.goeasy.livechat.R
import io.goeasy.livechat.chat.Message


class MessageAdapter(private var messageList: MutableList<Message> ) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

    inner class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        var nameView:TextView=view.findViewById(R.id.name)
        var contentView:TextView=view.findViewById(R.id.content)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.list_item_message,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = messageList[position]
        holder.nameView.text = "${message.senderNickname}: "
        holder.contentView.text = "${message.content}"
    }

    override fun getItemCount(): Int {
        return messageList.size
    }
}