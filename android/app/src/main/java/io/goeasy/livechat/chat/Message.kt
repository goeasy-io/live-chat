package io.goeasy.livechat.chat

import org.json.JSONObject

data class Message(
    var content: String,
    var senderUserId: String,
    var senderNickname: String,
    var type: MessageType
) {
    override fun toString(): String {
        val json = JSONObject()
        json.put("content", content)
        json.put("senderUserId", senderUserId)
        json.put("senderNickname", senderNickname)
        json.put("type", type.value)
        return json.toString()
    }

    companion object {
        fun parseString(string: String): Message {
            val json = JSONObject(string)
            val content = json.getString("content")
            val senderUserId = json.getString("senderUserId")
            val senderNickname = json.getString("senderNickname")
            val typeValue = json.getInt("type")
            val type = MessageType.values().firstOrNull { it.value == typeValue } ?: MessageType.CHAT
            return Message(content, senderUserId, senderNickname, type)
        }
    }
}
