package io.goeasy.livechat.chat

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import io.goeasy.ConnectEventListener
import io.goeasy.ConnectOptions
import io.goeasy.GResult
import io.goeasy.GoEasy
import io.goeasy.GoEasyEventListener
import io.goeasy.livechat.MainActivity
import io.goeasy.livechat.R
import io.goeasy.livechat.adapter.AvatarListAdapter
import io.goeasy.livechat.adapter.MessageAdapter
import io.goeasy.pubsub.GPubSub
import io.goeasy.pubsub.PubSubMessage
import io.goeasy.pubsub.PublishOptions
import io.goeasy.pubsub.history.HistoryOptions
import io.goeasy.pubsub.presence.HereNowOptions
import io.goeasy.pubsub.presence.HereNowResponse
import io.goeasy.pubsub.presence.Member
import io.goeasy.pubsub.presence.SubscribePresenceOptions
import io.goeasy.pubsub.subscribe.SubscribeOptions

enum class MessageType(val value: Int) { CHAT(0), PROP(1) }
enum class PropType(val value: String) { HEART("0"), ROCKET("1") }

data class User(var id: String, var nickname: String, var avatarId: Int)
data class Room(var id: String, var name: String)
data class OnlineUsers(var count: Int, var users: MutableList<User>)

class ChatRoomActivity : ComponentActivity() {
    private lateinit var currentUser: User
    private lateinit var currentRoom: Room

    private lateinit var onlineUsers: OnlineUsers
    private lateinit var messages: MutableList<Message>

    private lateinit var avatarList: RecyclerView
    private lateinit var messageList: RecyclerView
    private lateinit var newMessageView: TextView

    private lateinit var rocketView: ImageView
    private lateinit var heartView: ImageView

    private lateinit var avatarAdapter: AvatarListAdapter
    private lateinit var messageAdapter: MessageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chatroom)

        //接收传值
        initData()
        //初始化视图
        initView()
        //初始化GoEasy
        initGoEasy()

    }

    private fun initData() {
        val id = intent.getStringExtra("id").toString()
        val nickname = intent.getStringExtra("nickname").toString()
        val avatarId = intent.getIntExtra("avatarId", 0)
        val roomId = intent.getStringExtra("roomId").toString()
        val roomName = intent.getStringExtra("roomName").toString()
        currentUser = User(id,nickname, avatarId)
        currentRoom = Room(roomId, roomName)
        onlineUsers = OnlineUsers(0, mutableListOf())
        messages = mutableListOf()
    }

    private fun initView() {
        newMessageView = findViewById(R.id.message_input)
        rocketView = findViewById(R.id.rocket)
        heartView = findViewById(R.id.heart)
        val nameView: TextView = findViewById(R.id.room_name)
        nameView.text = currentRoom.name

        val countView = findViewById<TextView>(R.id.online_count)
        countView.text = onlineUsers.count.toString()

        avatarList = findViewById(R.id.avatarList)
        avatarAdapter = AvatarListAdapter(onlineUsers.users)
        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.justifyContent = JustifyContent.FLEX_END
        avatarList.layoutManager = layoutManager
        avatarList.adapter = avatarAdapter

        messageList = findViewById(R.id.messageList)
        messageList.layoutManager = LinearLayoutManager(this)
        messageAdapter = MessageAdapter(messages)
        messageList.adapter = messageAdapter

    }


    private fun initGoEasy() {
        // 初始化GoEasy
        GoEasy.init(GoEasyConfig.host, GoEasyConfig.appkey, this.applicationContext)
        // 连接GoEasy
        connect()
        // 订阅上下线
        subscribePresence()
        // 订阅channel
        subscribe(onMessage)
        // 历史消息
        history()
        // 在线用户
        hereNow()
    }

    fun connect() {
        val connectOptions = ConnectOptions()
        connectOptions.id = currentUser.id
        connectOptions.data = mapOf("nickname" to currentUser.nickname, "avatarId" to currentUser.avatarId.toString())
        GoEasy.connect(connectOptions, object : ConnectEventListener() {
            override fun onSuccess(data: GResult<*>) {
                Log.i("chatroom","连接成功")
            }
            override fun onFailed(error: GResult<*>) {
                Log.i("chatroom","Failed to connect GoEasy, code:" + error.code + ",error:" + error.data);
            }
            override fun onProgress(attempts: Int) {
                Log.i("chatroom", "GoEasy connect progress attempts: $attempts")
            }
        })
    }

    fun subscribe(callback: (String) -> Unit) {
        val options = SubscribeOptions(
            channel = currentRoom.id,
            presence = mapOf("enable" to true),
            onMessage = { message: PubSubMessage ->
                Log.i("chatroom","Message content: ${message.content}")
                callback(message.content)
            },
            onSuccess = {
                Log.i("chatroom","订阅成功")
            },
            onFailed = { error ->
                Log.i("chatroom","Subscribe failed: code: ${error.code} data: ${error.data}")
            }
        )
        GPubSub.subscribe(options)
    }

    private val onMessage = { content: String ->
        runOnUiThread {
            val message = Message.parseString(content)
            if (message.type == MessageType.PROP) {
                if (message.content == PropType.HEART.value) {
                    message.content = "送出了一个大大的比心❤️"
                    playAnimation(PropType.HEART)
                } else if (message.content == PropType.ROCKET.value) {
                    message.content = "送出了一枚大火箭🚀"
                    playAnimation(PropType.ROCKET)
                }
            }
            messages.add(message)
            messageAdapter.notifyItemRangeChanged(messageAdapter.itemCount, messages.size)
            scrollToBottom()
        }
    }

    fun subscribePresence() {
        val options = SubscribePresenceOptions(
            channel = currentRoom.id,
            onPresence = {event ->
                runOnUiThread {
                    val members = convertMembersToUsers(event.members)
                    onlineUsers.count = event.amount
                    onlineUsers.users = members
                    val countView = findViewById<TextView>(R.id.online_count)
                    countView.text = onlineUsers.count.toString()
                    avatarAdapter.updateUserList(onlineUsers.users)

                    val content = when (event.action) {
                        "join", "back" -> "进入房间"
                        else -> "退出房间"
                    }
                    val data = event.member.data as Map<*, *>
                    val nickname = data["nickname"].toString().replace("\"", "")
                    messages.add(Message(content, event.member.id, nickname, MessageType.CHAT))
                    messageAdapter.notifyItemRangeChanged(messageAdapter.itemCount, messages.size)
                    scrollToBottom()
                }
            },
            onSuccess = {
                Log.i("chatroom","subscribePresence success")
            },
            onFailed = { error ->
                Log.i("chatroom","subscribePresence failed: code: ${error.code} data: ${error.data}")
            }
        )
        GPubSub.subscribePresence(options)
    }

    private fun convertMembersToUsers(list: List<Member>): MutableList<User> {
        val members = mutableListOf<User>()
        for (member in list) {
            val data = member.data as Map<*, *>
            val nickname = data["nickname"].toString()
            var avatarId = data["avatarId"].toString()
            avatarId = avatarId.replace("\"", "")
            val user = User(member.id, nickname, avatarId.toInt())
            members.add(user)
        }
        return members
    }

    fun hereNow() {
        val options = HereNowOptions(
            channel = currentRoom.id,
            limit = 12,
            onSuccess = { data: HereNowResponse ->
                Log.i("chatroom","hereNow success: code: ${data.code} data: ${data.content}");
                runOnUiThread {
                    val members = convertMembersToUsers(data.content.members)
                    onlineUsers.count = data.content.amount
                    onlineUsers.users = members
                }
            },
            onFailed = { error ->
                Log.i("chatroom","hereNow failed: code: ${error.code} data: ${error.data}")
            }
        )
        GPubSub.hereNow(options)
    }

    fun history() {
        val options = HistoryOptions(
            channel = currentRoom.id,
            limit = 10,
            onSuccess = { data ->
                runOnUiThread {
                    data.content.messages.map { message ->
                        val newMessage = Message.parseString(message.content)
                        if (newMessage.type == MessageType.PROP) {
                            newMessage.content = when (newMessage.content) {
                                PropType.HEART.value -> "送出了一个大大的比心❤️"
                                PropType.ROCKET.value -> "送出了一枚大火箭🚀"
                                else -> newMessage.content
                            }
                        }
                        messages.add(newMessage)
                    }
                    messageAdapter.notifyItemRangeChanged(messageAdapter.itemCount, messages.size)
                    Log.i("chatroom", "history success: messages: ${messages}");
                }
            },
            onFailed = { error ->
                Log.i("chatroom","history failed: code: ${error.code} data: ${error.data}")
            }
        )
        GPubSub.history(options)
    }

    fun sendTextMessage(view: View) {
        val content = newMessageView.text.toString()
        if (content == "") {
            return;
        }
        sendMessage(Message(content, currentUser.id, currentUser.nickname, MessageType.CHAT))
        newMessageView.text = ""
    }

    fun sendPropMessage(view: View) {
        when (view.id) {
            R.id.rocket_button -> {
                playAnimation(PropType.ROCKET)
                sendMessage(Message(PropType.ROCKET.value, currentUser.id, currentUser.nickname, MessageType.PROP))
            }
            R.id.heart_button -> {
                playAnimation(PropType.HEART)
                sendMessage(Message(PropType.HEART.value, currentUser.id, currentUser.nickname, MessageType.PROP))
            }
        }
    }

    private fun sendMessage(message: Message) {
        val options = PublishOptions(currentRoom.id, message.toString(), 1)
        GPubSub.publish(options, object : GoEasyEventListener() {
            override fun onSuccess(data: GResult<*>) {
                Log.i("chatroom", "消息发送成功: code: ${data.code} data: ${data.data}")
            }

            override fun onFailed(error: GResult<*>) {
                Log.i("chatroom","消息发送失败，错误编码：" + error.code + " 错误信息：" + error.data);
            }
        })
    }

    private fun playAnimation(prop: PropType) {
        val view = if (prop == PropType.HEART) heartView else rocketView
        view.visibility = View.VISIBLE
        view.animate().translationY(-1500f).setDuration(1500).withEndAction {
            view.visibility = View.GONE
            view.translationY = 0f
        }
    }

    private fun scrollToBottom() {
        runOnUiThread {
            messageList.post {
                val targetPosition = messageAdapter.itemCount - 1
                if (targetPosition >= 0) {
                    messageList.smoothScrollToPosition(targetPosition)
                }
            }
        }
    }

    fun quitRoom(view: View) {
        GoEasy.disconnect(object : GoEasyEventListener() {
            override fun onSuccess(data: GResult<*>) {
                Log.i("chatroom","disconnect success: code: ${data.code} data: ${data.data}")
                val intent = Intent(this@ChatRoomActivity, MainActivity::class.java)
                startActivity(intent)
            }

            override fun onFailed(error: GResult<*>) {
                Log.i("chatroom","disconnect failed: code: ${error.code} data: ${error.data}")
            }
        })
    }
}
