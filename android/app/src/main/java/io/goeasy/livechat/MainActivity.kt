package io.goeasy.livechat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import io.goeasy.livechat.adapter.AvatarAdapter
import io.goeasy.livechat.adapter.RoomAdapter
import io.goeasy.livechat.chat.ChatRoomActivity


data class Avatar(val id: String, val avatarId: Int)
data class Room(val id: String, val name: String)

class MainActivity : AppCompatActivity() {

    private lateinit var avatarView: RecyclerView
    private lateinit var roomView: RecyclerView
    private var avatarList: List<Avatar> = listOf(
        Avatar("1", R.drawable.avatar1),
        Avatar("2", R.drawable.avatar2),
        Avatar("3", R.drawable.avatar3),
        Avatar("4", R.drawable.avatar4),
        Avatar("5", R.drawable.avatar5),
        Avatar("6", R.drawable.avatar6),
        Avatar("7", R.drawable.avatar7),
        Avatar("8", R.drawable.avatar8),
    )
    private var roomList: List<Room> = listOf(
        Room("001", "程序员集散地"),
        Room("002", "舌尖上的中国"),
        Room("003", "驴友之家"),
        Room("004", "球迷乐翻天"),
    )

    private var nickname: EditText? = null
    private var selectedAvatar: Avatar? = null
    private var selectedRoom: Room? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nickname = findViewById(R.id.nickname)
        avatarView = findViewById(R.id.avatarView)
        roomView = findViewById(R.id.roomView)

        initAdapter()
    }

    private fun initAdapter() {
        avatarView.layoutManager = initFlexManager()
        val avatarAdapter = avatarList.let { AvatarAdapter(it) }
        avatarView.adapter = avatarAdapter

        avatarAdapter.setOnItemClickListener(object :AvatarAdapter.OnItemClickListener{
            override fun onClick(view: View, position: Int) = onClickAvatar(view, position)
        })

        roomView.layoutManager = initFlexManager()
        val roomAdapter = roomList.let { RoomAdapter(it) }
        roomView.adapter = roomAdapter

        roomAdapter.setOnItemClickListener(object :RoomAdapter.OnRoomClickListener{
            override fun onClick(view: View, position: Int) = onClickRoom(view, position)
        })
    }

    fun onClickAvatar(view: View, position: Int) {
        val avatar= avatarList[position]
        selectedAvatar = avatar
        view.hideKeyboard()
    }

    fun onClickRoom(view: View, position: Int) {
        val room= roomList[position]
        selectedRoom = room

        if (nickname?.text.toString().isEmpty()) {
            Toast.makeText(this.applicationContext,"请先输入昵称",Toast.LENGTH_SHORT).show()
            return
        }
        if (selectedAvatar == null) {
            Toast.makeText(this.applicationContext,"请选择一个头像",Toast.LENGTH_SHORT).show()
            return
        }

        val intent: Intent =  Intent(this@MainActivity, ChatRoomActivity::class.java).apply {
            putExtra("roomId", selectedRoom!!.id)
            putExtra("roomName", selectedRoom!!.name)
            putExtra("id", (Math.random() * 1000).toString())
            putExtra("nickname", nickname?.text.toString())
            putExtra("avatarId", selectedAvatar!!.avatarId)
        }


        nickname?.setText("")
        startActivity(intent)
    }

    private fun View.hideKeyboard() {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun initFlexManager(): FlexboxLayoutManager {
        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.flexWrap = FlexWrap.WRAP
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.alignItems = AlignItems.STRETCH
        return layoutManager
    }

    override fun onResume() {
        super.onResume()
        initAdapter()
    }

}



