package io.goeasy.livechat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import io.goeasy.livechat.R
import io.goeasy.livechat.chat.User

class AvatarListAdapter(private var userList: MutableList<User>) : RecyclerView.Adapter<AvatarListAdapter.ViewHolder>() {
    init {
        // 设置横向布局
        setHasStableIds(true)
        super.setHasStableIds(true)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User) {
            val avatarImageView = itemView.findViewById<ImageView>(R.id.item_avatar)
            avatarImageView.setImageResource(user.avatarId)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_overlap_avatar, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userList[position]
        holder.bind(user)

        // 设置重叠效果
        if (position > 0) {
            val layoutParams = holder.itemView.layoutParams as ViewGroup.MarginLayoutParams
            layoutParams.leftMargin = -40  // 负值表示重叠部分的宽度
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun updateUserList(newList: MutableList<User>) {
        userList = newList
        notifyDataSetChanged()
    }

}
