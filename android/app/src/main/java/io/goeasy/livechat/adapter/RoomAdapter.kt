package io.goeasy.livechat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.goeasy.livechat.R
import io.goeasy.livechat.Room


class RoomAdapter(private var roomList: List<Room> ) : RecyclerView.Adapter<RoomAdapter.ViewHolder>() {
    private lateinit var onItemClickListener: OnRoomClickListener

    inner class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        var nameView:TextView=view.findViewById(R.id.room_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.list_item_room,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val room=roomList[position]
        holder.nameView.text = room.name

        onItemClickListener.let {
            holder.nameView.apply {
                setOnClickListener {
                    onItemClickListener.onClick(it, position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return roomList.size
    }

    fun setOnItemClickListener(onItemClickListener: OnRoomClickListener){
        this.onItemClickListener=onItemClickListener
    }

    interface OnRoomClickListener {
        fun onClick(view: View, position: Int)
    }
}