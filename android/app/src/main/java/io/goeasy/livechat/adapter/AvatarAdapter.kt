package io.goeasy.livechat.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import io.goeasy.livechat.R
import io.goeasy.livechat.Avatar


class AvatarAdapter(private var avatarList: List<Avatar> ) : RecyclerView.Adapter<AvatarAdapter.ViewHolder>() {
    private lateinit var onAvatarClickListener: OnItemClickListener
    private var selectedItemPosition: Int = RecyclerView.NO_POSITION


    inner class ViewHolder(view:View):RecyclerView.ViewHolder(view){
        var imageView:ImageView=view.findViewById(R.id.user_avatar)
    }

    //重写的第一个方法，用来给制定加载那个类型的Recycler布局
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.list_item_avatar,parent,false)
        return ViewHolder(view)
    }

    @SuppressLint("RecyclerView")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val avatar=avatarList[position]
        holder.imageView.setImageResource(avatar.avatarId)

        onAvatarClickListener.let {
            holder.imageView.apply {
                setOnClickListener {
                    val previousSelectedPosition = selectedItemPosition
                    selectedItemPosition = position
                    notifyItemChanged(previousSelectedPosition)
                    notifyItemChanged(selectedItemPosition)
                    onAvatarClickListener.onClick(it, position)
                }
                if (position == selectedItemPosition) {
                    this.setBackgroundResource(R.drawable.red_circle_border)
                } else {
                    this.setBackgroundResource(R.drawable.white_circle_border)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return avatarList.size
    }


    fun setOnItemClickListener(onAvatarClickListener: OnItemClickListener){
        this.onAvatarClickListener=onAvatarClickListener
    }

    interface OnItemClickListener {
        fun onClick(view: View, position: Int)
    }

    fun setSelectedItem(position: Int) {
        val previousSelectedPosition = selectedItemPosition
        selectedItemPosition = position
        notifyItemChanged(previousSelectedPosition)
        notifyItemChanged(selectedItemPosition)
    }

}
