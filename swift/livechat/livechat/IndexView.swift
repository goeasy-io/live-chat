import SwiftUI


struct IndexView: View {
    @Environment(\.presentationMode) private var presentationMode

    @State private var nickname = ""
    @State private var selectedAvatar = Avatar(id:"",avatarId: "")
    @State private var selectedRoom = Room(id: "", name: "")
    @State private var showChatRoom = false

    var body: some View {

        let avatarList: Array = [
            Avatar(id:"1",avatarId: "avatar01"),
            Avatar(id:"2",avatarId: "avatar02"),
            Avatar(id:"3",avatarId: "avatar03"),
            Avatar(id:"4",avatarId: "avatar04"),
            Avatar(id:"5",avatarId: "avatar05"),
            Avatar(id:"6",avatarId: "avatar06"),
            Avatar(id:"7",avatarId: "avatar07"),
            Avatar(id:"8",avatarId: "avatar08")
        ];

        let roomList: Array = [
            Room(id: "001", name: "程序员集散地"),
            Room(id: "002", name: "舌尖上的中国"),
            Room(id: "003", name: "驴友之家"),
            Room(id: "004", name: "球迷乐翻天")
        ]

        NavigationView {
            VStack {
                VStack(alignment: .center) {
                    Text("GoEasy Websocket示例")
                        .font(.title)
                        .foregroundColor(Color("GoEasyRed"))
                    Text("Swift 聊天室（直播间）")
                        .font(.title2)
                        .foregroundColor(Color("GoEasyRed"))
                }.padding(.bottom, 20)

                TextField("请输入昵称", text: $nickname)
                    .frame(width: 300.0, height: 40.0)
                    .multilineTextAlignment(.center)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color("GoEasyRed"), lineWidth: 2)
                    )

                Text("请选择头像")
                    .font(.system(size: 18))
                    .padding(.top, 20)
                    .frame(width: 300.0, alignment: .leading)

                LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
                    ForEach(avatarList, id: \.id) { avatar in
                        Image(avatar.avatarId)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 60, height: 60)
                            .clipShape(Circle())
                            .padding(5)
                            .cornerRadius(30)
                            .overlay(
                                Circle()
                                    .stroke(selectedAvatar.id == avatar.id ? Color("GoEasyRed") : Color.clear, lineWidth: 2)
                            )
                            .onTapGesture {
                                selectedAvatar = avatar
                                UIApplication.shared.hideKeyboard()
                            }
                    }
                }
                .frame(width: 300.0, height: 140.0)

                Text("请选择聊天室")
                    .font(.system(size: 18))
                    .padding(.top, 20)
                    .frame(width: 300.0, alignment: .leading)


                LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 10) {
                    ForEach(roomList, id: \.id) { room in
                        RoundedRectangle(cornerRadius: 20)
                            .fill(Color("GoEasyRed"))
                            .frame(width: 140, height: 40)
                            .overlay(
                                Text(room.name)
                                    .font(.system(size: 18))
                                    .foregroundColor(Color.white)
                            )
                            .onTapGesture {
                                selectedRoom = room
                                if (!nickname.isEmpty && !selectedAvatar.id.isEmpty) {
                                    showChatRoom = true
                                }
                            }
                    }
                    
                    NavigationLink(
                        destination: ChatRoom(
                            currentRoom: selectedRoom,
                            id: String(Float.random(in: 0...100)*1000),
                            nickname:nickname,
                            avatarId:selectedAvatar.avatarId
                        ),
                        isActive: $showChatRoom,
                        label: { EmptyView() }
                    )
                    .navigationBarBackButtonHidden(true)

                }
                .frame(width: 300.0, height: 80.0)
                .padding()

            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .ignoresSafeArea(.keyboard, edges: .bottom)
            .edgesIgnoringSafeArea(.all)
            .onAppear{
                nickname = ""
                selectedRoom = Room(id: "",name: "")
                selectedAvatar = Avatar(id: "", avatarId: "")
            }
        }

    }

}

struct IndexView_Previews: PreviewProvider {
    static var previews: some View {
        IndexView()
    }
}

struct Avatar : Identifiable {
    var id: String, avatarId: String
}

struct Room : Identifiable {
    var id: String, name: String
}
