import SwiftUI
import Combine
import GoEasySwift

extension UIApplication {
    func hideKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

class ViewModel: ObservableObject {
    static let viewModel = ViewModel()
    @Published var messages: [Message] = []
    @Published var onlineUsers: OnlineUsers = OnlineUsers(count: 0, users: [])
}


struct ChatRoom: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel = ViewModel.viewModel
    
    @State private var message = ""
    
    @State private var animateRocket = false
    @State private var animateHeart = false

    @State private var rocketOffset: CGFloat = 200
    @State private var heartOffset: CGFloat = 150

    let currentRoom: Room
    let id: String
    let nickname: String
    let avatarId: String
    
    var body: some View {

        
        VStack {
            // 头像列表
            HStack{
                Spacer()
                HStack(spacing: -15) {
                    ForEach(viewModel.onlineUsers.users, id: \.id) { user in
                        Image(user.avatarId)
                        .resizable()
                        .frame(width: 40, height: 40)
                        .clipShape(Circle())
                    }
                }

                ZStack {
                    Circle()
                        .foregroundColor(.gray)
                        .frame(width: 40, height: 40)
                    
                    Text(String(viewModel.onlineUsers.count))
                        .font(.system(size: 18))
                        .foregroundColor(.white)
                }
            }
            

            // 消息列表
            ScrollViewReader { scrollView in
                ScrollView {
                    VStack (alignment: .leading) {
                        ForEach(viewModel.messages, id: \.self) { message in
                            HStack{
                                Text("\(message.senderNickname):")
                                    .font(.system(size: 18))
                                    .foregroundColor(Color("GoEasyRed"))
                                Text("\(message.content)")
                                    .font(.system(size: 18))
                                    .foregroundColor( message.senderUserId == id ? Color("GoEasyRed") : Color(.black))
                            }
                            .padding(10)
                            .background(Color.gray.opacity(0.2))
                            .cornerRadius(20)
                            .onAppear() {
                                scrollView.scrollTo(message)
                            }
                        }
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, 15)
                    .onChange(of: viewModel.messages.count) { count in
                        scrollView.scrollTo(viewModel.messages.last)
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
            }


            // 操作栏
            HStack {
                HStack {
                    TextField("说点什么...", text: $message)
                        .font(.system(size: 18))

                    Button(action: {
                        if (!self.message.isEmpty) {
                            self.sendTextMessage()
                        }
                    }) {
                        Image(systemName: "arrow.up")
                            .font(.system(size: 20))
                            .frame(width: 40, height: 40)
                            .background(Color("GoEasyRed"))
                            .foregroundColor(.white)
                            .clipShape(Circle())
                    }

                }
                .padding(5)
                .background(Color.gray.opacity(0.2))
                .cornerRadius(25)

                Button(action: {
                    self.sendPropMessage(with: .heart)
                }) {
                    Image("handleheart")
                        .resizable()
                }.frame(width: 40, height: 40, alignment: .center)
                
                Button(action: {
                    self.sendPropMessage(with: .rocket)
                }) {
                    Image("rocket")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                }.frame(width: 40, height: 40, alignment: .center)
            }
            
            
        }
        .padding(10)
        .overlay(
            // 动画
            VStack {
                if animateRocket {
                    Image("rocket")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 100)
                        .offset(y: rocketOffset)
                        .onAppear { rocketOffset -= 800.0 }
                        .animation(Animation.easeInOut(duration: 2.0), value: rocketOffset)
                }
                
                if animateHeart {
                    VStack (spacing: -15) {
                        ForEach(0..<4) {index in
                            Image("heart").resizable().frame(width:60, height: 60)
                        }
                    }
                    .offset(y: heartOffset)
                    .onAppear { heartOffset -= 800.0 }
                    .animation(Animation.easeInOut(duration: 2.0), value: heartOffset)
                }
            }
        )
        .navigationBarTitle(currentRoom.name, displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            viewModel.messages = []
            viewModel.onlineUsers = OnlineUsers(count: 0, users: [])
            disconnect()
        }, label: {
            Image(systemName: "arrow.left")
            .foregroundColor(.black)
        }))
        .frame(maxWidth: .infinity, alignment: .center)
        .onAppear {
            initGoEasy()
            connectGoEasy()
            listenUsersOnlineOffline()
            loadHistory()
        }
    }
    
    func initGoEasy() {
        GoEasy.initGoEasy(host: GoEasyConfig.host, appkey: GoEasyConfig.appkey)
    }

    func connectGoEasy() {
        let connectOptions = ConnectOptions(
            id: self.id,
            data: ["nickname": self.nickname, "avatarId": self.avatarId]
        )

        let connectEventListener = ConnectEventListener()
        connectEventListener.onSuccess = { result in
            print("连接成功result：\(result.code)")
        }
        connectEventListener.onFailed = { error in
            print("连接失败：code:\(error.code) data:\(error.data)")
        }
        connectEventListener.onProgress = { attempts in
            print("尝试重连次数:\(attempts)")
        }
        GoEasy.connect(options: connectOptions, connectEventListener: connectEventListener)
    }
    
    func listenNewMessage() {
        let subscribeOption = SubscribeOptions(
            channel: currentRoom.id,
            presence: ["enable":true],
            onMessage: { message in
                print("onMessage:\(message.content)")
                let decodedMessage = Message.fromJSONString(message.content)
                viewModel.messages.append(decodedMessage)
            },
            onSuccess:{
                print("订阅成功")
            },
            onFailed:{ result in
                print("订阅失败: code:\(result.code) data:\(result.data)")
            }
        )
        GPubSub.subscribe(options: subscribeOption)
    }
    
    func unsubscribe() {
        let unsubscribeOption = UnSubscribeOptions(
            channel: currentRoom.id,
            onSuccess:{
                print("取消订阅成功")
            },
            onFailed:{ result in
                print("取消订阅失败: code:\(result.code) data:\(result.data)")
            }
        )
        GPubSub.unsubscribe(options: unsubscribeOption)
    }

    
    func listenUsersOnlineOffline() {
        let option = SubscribePresenceOptions(
            channel: currentRoom.id,
            onPresence: { event in
                var members: [User] = []
                for member in event.members {
                    let nickname = member.data["nickname"] as? String ?? ""
                    let avatarId = member.data["avatarId"] as? String ?? ""
                    
                    let user = User(id: member.id, nickname: nickname, avatarId: avatarId)
                    members.append(user)
                }
                
                viewModel.onlineUsers.count = event.amount
                viewModel.onlineUsers.users = members
                
                var content: String
                if event.action == "join" || event.action == "back" {
                    content = "进入房间"
                } else {
                    content = "退出房间"
                }
                let message = Message(content: content, senderUserId: id, senderNickname: nickname, type: MessageType.CHAT)
                viewModel.messages.append(message)

            },
            onSuccess:{
                listenNewMessage()
                loadOnlineUsers()
            },
            onFailed:{ result in
                print("订阅上下线失败: code:\(result.code) data:\(result.data)")
            }
        )
        GPubSub.subscribePresence(options: option)
        
    }
    
    func unSubscribePresence() {
        let option = UnSubscribePresenceOptions(
            channel: currentRoom.id,
            onSuccess:{
                print("取消订阅上下线成功")
            },
            onFailed:{ result in
                print("取消订阅上下线失败: code:\(result.code) data:\(result.data)")
            }
        )
        GPubSub.unsubscribePresence(options: option)
    }
    
    func loadOnlineUsers() {
        let option = HereNowOptions(
            channel: currentRoom.id,
            onSuccess:{ result in
                
                var members: [User] = []
                for member in result.content.members {
                    let nickname = member.data["nickname"] as? String ?? ""
                    let avatarId = member.data["avatarId"] as? String ?? ""
                    
                    let user = User(id: member.id, nickname: nickname, avatarId: avatarId)
                    members.append(user)
                }
                
                viewModel.onlineUsers.count = result.content.amount
                viewModel.onlineUsers.users = members
            },
            onFailed:{ result in
                print("herenow fail: code:\(result.code) data:\(result.data)")
            }
        )
        GPubSub.hereNow(options: option)
    }
    
    func loadHistory() {
        let option = HistoryOptions(
            channel: currentRoom.id,
            limit: 10,
            onSuccess:{ result in
                var messages: [Message] = []
                for message in result.content.messages {
                    let decodedMessage = Message.fromJSONString(message.content)
                    messages.append(decodedMessage)
                }
                viewModel.messages = messages
                print("loadHistory success:\(messages)")
                
            },
            onFailed:{ result in
                print("loadHistory fail: code:\(result.code) data:\(result.data)")
            }
        )
        GPubSub.history(options: option)
    }
    
    func disconnect() {
        let disconnectEventListener = GoEasyEventListener()
        disconnectEventListener.onSuccess = { result in
            print("断开连接成功.")
            presentationMode.wrappedValue.dismiss()
        }
        disconnectEventListener.onFailed = { result in
            print("断开连接失败 code:\(result.code) data:\(result.data)")
        }
        GoEasy.disconnect(disconnectEventListener: disconnectEventListener)
    }

    func sendTextMessage() {
        UIApplication.shared.hideKeyboard()
        self.sendMessage(messageType: MessageType.CHAT, content: message)
        message = ""
    }

    func sendPropMessage(with type: PropType) {
        self.playAnimation(type: type)
        let content = type == .heart ? "送出了一个大大的比心" : "送出了一枚大火箭"
        self.sendMessage(messageType: MessageType.PROP, content: content)
    }
    
    func sendMessage(messageType: MessageType, content: String) {
        let newMessage = Message(content: content, senderUserId: id, senderNickname: nickname, type: messageType)
        
        let publishEventListener = GoEasyEventListener()
        publishEventListener.onSuccess = { result in
            print("发送成功 code:\(result.code) data:\(result.data)")
            message = ""
        }
        publishEventListener.onFailed = { result in
            print("发送失败: code:\(result.code) data:\(result.data)")
        }
        let options = PublishOptions(channel: currentRoom.id, message: newMessage.toJSONString(), qos: 1)
        GPubSub.publish(options: options, publishEventListener: publishEventListener)
    }
    
    func playAnimation(type: PropType) {
        switch type {
            case .heart:
            withAnimation {
                if (animateHeart) {
                    return
                }
                animateHeart = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    animateHeart = false
                    heartOffset = 150
                }
            }
            case .rocket:
                withAnimation {
                    if (animateRocket) {
                        return
                    }
                    animateRocket = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        animateRocket = false
                        rocketOffset = 200
                    }
                }
        }
    }
}

struct ChatRoom_Previews: PreviewProvider {
    static var previews: some View {
        ChatRoom(
            currentRoom: Room(id: "001", name: "程序员集散地"),
            id: "betty01",
            nickname:"betty",
            avatarId:"avatar02"
        )
    }
}

enum PropType: String {
    case heart = "0"
    case rocket = "1"
}

enum MessageType: Int, Codable {
    case CHAT = 0
    case PROP = 1
}

struct User {
    var id: String, nickname: String, avatarId: String
}

struct OnlineUsers {
    var count: Int32, users: [User]
}

struct Message: Hashable, Codable {
    let content: String
    let senderUserId: String
    let senderNickname: String
    let type: MessageType

    func toJSONString() -> String {
        let jsonData = try! JSONEncoder().encode(self)
        return String(data: jsonData, encoding: .utf8)!
    }

    static func fromJSONString(_ jsonString: String) -> Message {
        let jsonData = jsonString.data(using: .utf8)!
        do {
            return try JSONDecoder().decode(Message.self, from: jsonData)
        } catch {
            return Message(content: jsonString, senderUserId: "unknow", senderNickname: "unknow", type: .CHAT)
        }
    }
    
}

struct Animate {
    var offset: CGFloat, play: Bool
}


