import SwiftUI

@main
struct livechatApp: App {
    var body: some Scene {
        WindowGroup {
            IndexView()
        }
    }
}
