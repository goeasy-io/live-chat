const goEasy = getApp().globalData.goEasy;
const pubSub = goEasy.pubsub;

Page({
	data: {
		currentRoom: null,
		// 道路展示
		propDisplay: {
			showPropType: 0,
			play: false,
			timer: null
		},
		newMessageContent: "",
		// 道具类型
		Prop: {
			HEART: 0,//桃心
			ROCKET: 1//火箭
		},
		// 消息类型
		MessageType: {
			CHAT: 0,//文字聊天
			PROP: 1//道具
		}
	},
	onLoad: async function(options) {
		const roomToken = JSON.parse(options.roomToken);
		// 初始化room
		this.setData({
			currentRoom: {
				roomId: roomToken.roomId,
				roomName: roomToken.roomName,
				onlineUsers: {
					amount: 0,
					users: []
				},
				messages: [],
				currentUser: {
					id: roomToken.userId,
					nickname: roomToken.nickname,
					avatar: roomToken.avatar
				}
			}
		});
		wx.setNavigationBarTitle({
			title: roomToken.roomName
		});
		// 连接goEasy
		this.connectGoEasy();

		// 监听用户上下线
		await this.listenUsersOnlineOffline();

		// 监听新消息
		this.listenNewMessage();
		// 加载最后10条消息历史
		this.loadHistory();

		// 加载在线用户列表
		await this.loadOnlineUsers();
	},
	onUnload(){
		// 退出聊天室
		goEasy.disconnect({
			onSuccess(){
				console.log("GoEasy disconnect successfully");
			},
			onFailed(error){
				console.log("GoEasy disconnect failed"+JSON.stringify(error));
			}
		});
	},
	// 连接goEasy
	connectGoEasy(){
		const userData = {
			avatar: this.data.currentRoom.currentUser.avatar,
			nickname: this.data.currentRoom.currentUser.nickname
		}
		goEasy.connect({
			id : this.data.currentRoom.currentUser.id,
			data : userData,
			onSuccess: function(){
				console.log("GoEasy connect successfully.")
			},
			onFailed: function(error){
				console.log("Failed to connect GoEasy, code:"+error.code+ ",error:"+error.content);
			},
			onProgress: function(attempts){
				console.log("GoEasy is connecting", attempts);
			}
		});
	},
	// 监听用户上下线
	listenUsersOnlineOffline(){
		let self = this;
		const roomId = this.data.currentRoom.roomId;
		return new Promise((resolve, reject) => {
			pubSub.subscribePresence({
				channel: roomId,
				onPresence: function (presenceEvents) {
					self.setData({
						"currentRoom.onlineUsers.amount": presenceEvents.amount,
						"currentRoom.onlineUsers.users": presenceEvents.members,
					});
					let content;
					if (presenceEvents.action==="join" || presenceEvents.action==="back") {
						content = "进入房间";
					} else {
						content = "退出房间";
					}
					const message = {
						content: content,
						senderUserId: presenceEvents.member.id,
						senderNickname: presenceEvents.member.data.nickname,
						type: self.data.MessageType.CHAT
					};
					self.data.currentRoom.messages.push(message);
					self.setData({
						"currentRoom.messages": self.data.currentRoom.messages
					});
					self.scrollBottom();
				},
				onSuccess : function () {
					console.log("用户上下线监听成功")
					resolve();
				},
				onFailed : function (error) {
					console.log("监听用户上下线失败, code:" + error.code + ",content:" + error.content);
					reject(error);
				}
			})
		})
	},
	// 监听新消息
	listenNewMessage(){
		// 监听当前聊天室的消息
		let self = this;
		const roomId = this.data.currentRoom.roomId;
		pubSub.subscribe({
			channel: roomId,
			presence: { // 可选
				enable: true,
			},
			onMessage : function (message) {
				const content = JSON.parse(message.content);
                self.data.currentRoom.messages.push(content);
                self.setData({
                    "currentRoom.messages": self.data.currentRoom.messages
                });
				if (content.type === self.data.MessageType.PROP) {
					self.propAnimation(parseInt(content.content))
				}
				self.scrollBottom();
			},
			onSuccess : function () {
				console.log("监听新消息成功")
			},
			onFailed : function(error) {
				console.log("订阅消息失败, code:"+error.code+ ",错误信息:"+error.content);
			}
		})
	},
	// 加载在线用户列表
	loadOnlineUsers(){
		let self = this;
		const roomId = this.data.currentRoom.roomId;
		return new Promise((resolve, reject) => {
			pubSub.hereNow({
				channel: roomId,
				onSuccess:function (result) {
					self.setData({
						"currentRoom.onlineUsers.amount": result.content.amount,
						"currentRoom.onlineUsers.users": result.content.members,
					});
					resolve(result);
				},
				onFailed: function (error) {
					console.log("获取在线用户失败, code:" + error.code + ",错误信息:" + error.content);
					reject(error);
				}
			})
		})
	},
	// 加载最后10条消息历史
	loadHistory(){
		let self = this;
		const roomId = this.data.currentRoom.roomId;
		pubSub.history({
			channel: roomId, //必需项
			limit: 10, //可选项，返回的消息条数
			onSuccess:function(response) {
				let messages = [];
				response.content.messages.map(message => {
					const historyMessage = JSON.parse(message.content);
					messages.push(historyMessage);
				});
				self.setData({
					"currentRoom.messages": messages
				});
			},
			onFailed: function (error) {
				//获取失败
				console.log("获取历史消息失败, code:" + error.code + ",错误信息:" + error.content);
			}
		});
	},
	onInputMessage(event) { //双向绑定消息 兼容
		this.setData({
			newMessageContent: event.detail.value
		})
	},
	sendMessage(event) {
		//发送消息
		let content = event.target.dataset.content;
		let messageType = event.target.dataset.messagetype;
		if(messageType === this.data.MessageType.CHAT){
			if(this.data.newMessageContent === ""){
				return;
			}else {
				content = this.data.newMessageContent;
			}
		}
		const message = {
			senderNickname: this.data.currentRoom.currentUser.nickname,
			senderUserId: this.data.currentRoom.currentUser.id,
			type: messageType,
			content: content
		};
		pubSub.publish({
			channel : this.data.currentRoom.roomId,
			message : JSON.stringify(message),
			onSuccess : function () {
				console.log("发送成功");
			},
			onFailed : function (error) {
				console.log("消息发送失败，错误编码：" + error.code + " 错误信息：" + error.content);
			}
		});
		this.setData({
			newMessageContent: ""
		});
	},
	propAnimation(type) { //道具动画
		//动画的实现
		if (this.data.propDisplay.timer) {
			return;
		}
		this.setData({
			'propDisplay.showPropType': type,
			'propDisplay.play': true,
			'propDisplay.timer': setTimeout(() => {
				this.setData({
					'propDisplay.play': false,
					'propDisplay.timer': null
				})
			}, 2000)
		});
	},
	scrollBottom(){
		// 滑动到最底部
		wx.pageScrollTo({
			scrollTop : 200000,
			duration :10
		});
	}
});
